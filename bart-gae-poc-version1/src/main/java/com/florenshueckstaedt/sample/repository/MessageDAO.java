package com.florenshueckstaedt.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.florenshueckstaedt.sample.domain.Message;

public interface MessageDAO extends JpaRepository<Message,Long>{

}
