package com.florenshueckstaedt.sample.web;




import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;

import com.florenshueckstaedt.sample.domain.Message;
import com.florenshueckstaedt.sample.repository.MessageDAO;
import com.florenshueckstaedt.sample.service.TestInterface;
import com.florenshueckstaedt.sample.service.internal.ApplicationContextProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Configurable
public class MyApplicationUI extends UI {

	private TestInterface ti;
    
    
    private ApplicationContext getApplicationContext(){
		return ApplicationContextProvider.getApplicationContext();
    	
    }

    
	@Override
	protected void init(VaadinRequest request) {
		
		VerticalLayout view = new VerticalLayout();
        setContent(view);
        
		VerticalLayout border = new VerticalLayout();
		
		// Set the root layout for the UI
		VerticalLayout content = new VerticalLayout();
		view.addComponent(border);
		 
		border.addComponent(content);
		border.setComponentAlignment(content, Alignment.MIDDLE_CENTER);
		content.setWidth("70%");
		
		// Add the topmost component.
		Label headline = new Label("Test Application");
		headline.setStyleName("h1");
		content.addComponent(headline);
		
		 
		// Add a horizontal layout for the bottom part.
		VerticalLayout bottom = new VerticalLayout();
		content.addComponent(bottom);
		bottom.setWidth("100%");
		

		String test = getApplicationContext().getId();
		Label testLabel = new Label(test);
		
		bottom.addComponent(testLabel);
		
		
		ti = getApplicationContext().getBean(TestInterface.class);
		Label testLabel2 = new Label(ti.getName());
		
		bottom.addComponent(testLabel2);
		
		

		MessageDAO dao = getApplicationContext().getBean(MessageDAO.class);
		
		Message m = new Message();
		m.setText("BeispielText");
		dao.save(m);
		
		List<Message> result = dao.findAll();
		for(Message me : result){
			Label l = new Label(me.getText()+me.getId());
			bottom.addComponent(l);
		}
		
	}
}
