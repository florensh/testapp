package com.florenshueckstaedt.sample.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Message implements Serializable{
	
	
	 @Id
	  @GeneratedValue
	  private Long id;

	  public Long getId() {return id;}

	  public void setId(Long id) {
	    this.id = id;
	  }
	  
	  private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
