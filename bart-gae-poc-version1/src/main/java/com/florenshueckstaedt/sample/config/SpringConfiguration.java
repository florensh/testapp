package com.florenshueckstaedt.sample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

import com.florenshueckstaedt.sample.service.TestInterface;
import com.florenshueckstaedt.sample.service.internal.ApplicationContextProvider;
import com.florenshueckstaedt.sample.service.internal.MyLogger;
import com.florenshueckstaedt.sample.service.internal.TestInterfaceImpl;

@Configuration
@EnableSpringConfigured
@EnableAspectJAutoProxy
public class SpringConfiguration {

	@Bean
	public ApplicationContextProvider applicationContextProvider() {
		ApplicationContextProvider applicationContextProvider = new ApplicationContextProvider();
		return applicationContextProvider;
	}
	
	@Bean
	public MyLogger myAspect(){
		MyLogger myAspect = new MyLogger();
		return myAspect;
	}
	
	@Bean
	public TestInterface testInterface(){
		TestInterfaceImpl ti = new TestInterfaceImpl();
		return ti;
	}
	
}