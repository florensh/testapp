package com.florenshueckstaedt.sample.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.florenshueckstaedt.sample.service.internal.MyLogger;

@Configuration
@EnableJpaRepositories("com.florenshueckstaedt.sample.repository")
@EnableTransactionManagement
class JpaApplicationConfig {
	 private static final Logger logger = LoggerFactory.getLogger(MyLogger.class);

	 @Bean
	 public AbstractEntityManagerFactoryBean entityManagerFactory() {

		 LocalEntityManagerFactoryBean factory = new LocalEntityManagerFactoryBean();
		 factory.setPersistenceUnitName("transactions-optional");
		 return factory;
	 }
	 
	 @Bean
	 public JpaTransactionManager transactionManager() throws ClassNotFoundException {
	     JpaTransactionManager transactionManager = new JpaTransactionManager();

	     transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

	     return transactionManager;
	 }
	 
}