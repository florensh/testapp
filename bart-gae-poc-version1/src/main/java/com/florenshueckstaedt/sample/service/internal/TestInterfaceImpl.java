package com.florenshueckstaedt.sample.service.internal;

import java.io.Serializable;

import com.florenshueckstaedt.sample.service.TestInterface;
import com.google.appengine.api.utils.SystemProperty;

public class TestInterfaceImpl implements TestInterface, Serializable{
	
	public String getName(){
		return SystemProperty.environment.value().toString();
	}

}
